import express from 'express';
import path from 'path';
import fs from 'fs';
import https from 'https';

const app = express();
app.use(express.urlencoded({ extended: true }));
app.use(express.static('public'));
app.set("views", path.join(__dirname, "views"));
app.set('view engine', 'pug');

const externalUrl = process.env.RENDER_EXTERNAL_URL;
const port = externalUrl && process.env.PORT ? parseInt(process.env.PORT) : 4050;

app.get('/',  function (req, res) {
    res.render('malicious-site');
});


// start server
if (externalUrl) {
  const hostname = '0.0.0.0';
  app.listen(port, hostname, () => {
  console.log(`Server locally running at http://${hostname}:${port}/ and from outside on ${externalUrl}`);
  });

} else {
  https.createServer({
      key: fs.readFileSync('server.key'),
      cert: fs.readFileSync('server.cert')
  }, app)
  .listen(port, function () {
      console.log(`Server running at https://localhost:${port}/`);
  });
}